# hyperia-commitizer

## Global Installation

For a quick global installation of the plugin, simply run the `install.sh` script present in this repo:

```
chmod +x install.sh

./install.sh
```

## Add this adapter

Install this adapter

```
npm install hyperia-commitizer
```

## Create `.cz.json`

```json
{
  "path": "node_modules/hyperia-commitizer"
}
```


## Run 

- with pre-commit `git cz`
- without pre-commit `git cz -n`

## Program process

after run:
- auto run command: `git add -A`
- question [list <type>]: Typ úkonu: `['task', 'bug', 'test', 'refactoring']`
- question [list <from>]: Odkial si vychádzal: `['develop', 'beta', 'rc-branch', 'master', 'other']`
- question [input <workflow>]: Skrátený popis práce:
- create commit from question and actual branch name in format:
```
<actualBranchName after />: <workflow> [<type> from <from>] (<actualBranchName>)
```
## Example
- actual branch: `task/LETKIM-5555`
- finnal commit: `LETKIM-5555: Short description [task from dev] (task/LETKIM-5555)`


